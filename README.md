# NgMapboxFirebase

# Scope

This project will provide base functionality and re-useable components for the following feature 
requirements:
- Basic mapping using Mapbox
- Route mapping
- Route directions through Mapbox Directions API
- Route Live tracker
- Integrate with provided GPX files
- Integration with Firebase Database Live updates 

# References
- For Firebase integration:
https://github.com/angular/angularfire2/blob/master/docs/install-and-setup.md [1]
- Mapbox Directions API:
https://www.mapbox.com/api-documentation/?language=cURL#directions [2]
- Mapbox integration with Angular:
https://angularfirebase.com/lessons/build-realtime-maps-in-angular-with-mapbox-gl/ [3]


# Original Angular-CLI Readme
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
