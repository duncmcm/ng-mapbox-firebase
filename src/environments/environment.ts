// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,

  mapbox: {
    accessToken: 'pk.eyJ1IjoiZHVuY21jbSIsImEiOiJjamFmYm82cWMxMm5nMzJyemg0YzFqODJvIn0.6qkOYFwZD25j8V26EZg2ng'
  }
  google: {
    API_KEY: 'AIzaSyAeD5HgVnr_txvlaoXtQ6e_RavtrM-YSrU',
  }
};
