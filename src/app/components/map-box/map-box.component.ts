import { Component, OnInit } from '@angular/core';
import {LngLat, Map} from 'mapbox-gl';
import { MapService } from '../../services/map.service';
import { GeoJson, FeatureCollection } from '../../models/map';
import {GeocodingService} from '../../services/geocoding.service';

@Component({
  selector: 'app-map-box',
  templateUrl: './map-box.component.html',
  styleUrls: ['./map-box.component.css']
})
export class MapBoxComponent implements OnInit {

  constructor(private mapService: MapService, private geocoder: GeocodingService ) { }

  ngOnInit() {
    const map = new Map({
      container: 'map',
      style: 'mapbox://styles/duncmcm/cjcdiz8t33lns2rqqferb00nb',
      zoom: 7,
      center: [-4.4243818, 57.3228575] /** Lng, Lat **/
    });

    this.mapService.map = map;
  }

  private initializeMap() {

  }

  buildMap() {

  }
}
