import { Component, OnInit } from '@angular/core';
import {GeocodingService} from '../../services/geocoding.service';
import {MapService} from '../../services/map.service';
import {Map} from 'mapbox-gl';

@Component({
  selector: 'app-navigator',
  templateUrl: './navigator.component.html',
  styleUrls: ['./navigator.component.css']
})
export class NavigatorComponent implements OnInit {

  private map: Map;
  private address: string;

  constructor(private geocoder: GeocodingService, private mapService: MapService) { }

  ngOnInit() {
    this.map = this.mapService.map;
    this.address = 'DL55PE';
    //this.goto(); /** NOTE: passing 'EH1' will not work for some reason **/
  }

  clear() {
    this.address = '';
  }

  goto() {
    if (!this.address) {
      return;
    }

    this.geocoder.geocode(this.address).subscribe(
      location => {
        this.map.fitBounds(location.viewBounds, {});
        this.address = location.address;
      },
      error => console.error(error));
  }

}
