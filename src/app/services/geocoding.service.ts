import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {LngLat, LngLatBounds} from 'mapbox-gl';
import {Location} from '../models/location';
import {environment} from '../../environments/environment';

import 'rxjs/add/operator/map';

@Injectable()
export class GeocodingService {

  http: Http;

  constructor(http: Http) {
    this.http = http;
  }

  geocode(address: string) {
    const url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&key=' + environment.google.API_KEY;
    console.log('Address ' + address + 'URL ' + url);

    return this.http
      .get(url)
      .map(res => res.json())
      .map(result => {
        if (result.status !== 'OK') { throw new Error('unable to geocode address'); }

        const location = new Location();
        location.address = result.results[0].formatted_address;
        location.latitude = result.results[0].geometry.location.lat;
        location.longitude = result.results[0].geometry.location.lng;

        const viewPort = result.results[0].geometry.viewport;
        location.viewBounds = new LngLatBounds(
          new LngLat(viewPort.southwest.lng, viewPort.southwest.lat),
          new LngLat(viewPort.northeast.lng, viewPort.northeast.lat)
        );

        return location;
      });
  }

  regeocode(lngLat: LngLat) {
    return this.http
      .get(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${lngLat.lat},${lngLat.lng}`)
      .map(res => res.json())
      .map(result => {
        if (result.status !== 'OK' || result.results.length < 1) { throw new Error('unable to geocode lat/lng'); }

        const location = new Location();
        location.address = result.results[0].formatted_address;
        location.latitude = lngLat.lat;
        location.longitude = lngLat.lng;

        return location;
      });
  }
}
