import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { GeoJson } from '../models/map';
import * as mapboxgl from 'mapbox-gl';
import { Map } from 'mapbox-gl';

@Injectable()
export class MapService {

  map: Map;

  constructor() {
    mapboxgl.accessToken = environment.mapbox.accessToken
  }

  getMarkers() {

  }
}
