import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {MapService} from './services/map.service';
import { MapBoxComponent } from './components/map-box/map-box.component';
import {GeocodingService} from './services/geocoding.service';
import { NavigatorComponent } from './components/navigator/navigator.component';

@NgModule({
  declarations: [
    AppComponent,
    MapBoxComponent,
    NavigatorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [MapService, GeocodingService],
  bootstrap: [AppComponent]
})
export class AppModule { }
