import { NgMapboxFirebasePage } from './app.po';

describe('ng-mapbox-firebase App', () => {
  let page: NgMapboxFirebasePage;

  beforeEach(() => {
    page = new NgMapboxFirebasePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
